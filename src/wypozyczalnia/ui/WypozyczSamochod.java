/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wypozyczalnia.ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.List;
import java.util.TimeZone;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import wypozyczalnia.entity.Klient;
import wypozyczalnia.entity.Samochod;
import wypozyczalnia.entity.Wypozyczenie;
import wypozyczalnia.util.HibernateUtil;

/**
 *
 * @author admin
 */
public class WypozyczSamochod extends javax.swing.JFrame {

    /**
     * Creates new form WypozyczSamochod
     */
    private String searchQuery = "from Samochod s where s.status=1";
    private ArrayList<Samochod> carList = new ArrayList<>();
    private Samochod chosenCar;
    private int sortType = -1;
    private static Klient k;
    public WypozyczSamochod(Klient k) {
        initComponents();
        this.k = k;
        jTable1.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);       
        setYears();
        getCars();
        getMarka();
        setSortComboBox();
    }

    private void setSortComboBox()
    {
        jComboBox1.addItemListener(new ItemListener(){
            @Override
            public void itemStateChanged(ItemEvent e) 
            {
                System.out.println("Zostało zmienione " + jComboBox1.getSelectedItem() + " "  + jComboBox1.getSelectedIndex());
                getSortType(carList);
                displayResults(carList);
            }
        });
    }
    private void getSortType(ArrayList<Samochod> list)
    {
        sortType = jComboBox1.getSelectedIndex();
        if(sortType == 0)
        {
            list.sort(Comparator.comparing(Samochod::getMarka));
        }
        else if(sortType == 1)
        {
            list.sort(Comparator.comparingInt(Samochod::getRokProdukcjiInt));
        }
        else if(sortType == 2)
        {
            list.sort(Comparator.comparingInt(Samochod::getRokProdukcjiInt).reversed());
        }
    }
    private void setYears()
    {
        int year = 1995;
        for(int i=0; i<26; i++)
        {
            jComboBox4.addItem(Integer.toString(year + i));
            jComboBox5.addItem(Integer.toString(year + i));
        }
    }
    private void getCars()
    {
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            String query = "from Samochod s where s.status=1";
            Query q = session.createQuery(query);
            List resultList = q.list();
            displayResults(resultList);
            carList = (ArrayList)resultList;
            session.getTransaction().commit();
        } catch (HibernateException he) {
            he.printStackTrace();
        }
    }
    private void getMarka()
    {
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            String query = "Select distinct marka from Samochod where status=1";
            Query q = session.createSQLQuery(query);
            List resultList = q.list();
            for(Object o : resultList)
            {
                jComboBox2.addItem(o.toString());
            }
            session.getTransaction().commit();
            jComboBox2.addActionListener(new ActionListener(){
                @Override
                public void actionPerformed(ActionEvent e) {
                    jComboBox3.setSelectedIndex(0);
                    int ItemCount = jComboBox3.getItemCount(); 
                    if(ItemCount>1)
                    for(int i=ItemCount-1; i>0; i--)
                    {
                        jComboBox3.removeItemAt(i);
                        System.out.println("Usuwane indeksy = " + i);
                    }
                    if(jComboBox2.getSelectedItem().equals("Dowolny"))
                    {
                        jComboBox3.setSelectedIndex(0);
                        jComboBox3.setEnabled(false);
                    }
                    else
                    {
                        jComboBox3.setEnabled(true);
                        Session session = HibernateUtil.getSessionFactory().openSession();
                        session.beginTransaction();
                        String query = "Select distinct model from Samochod where marka='" + jComboBox2.getSelectedItem()+ "'";
                        Query q = session.createSQLQuery(query);
                        List resultList = q.list();
                        for(Object o : resultList)
                        {
                            jComboBox3.addItem(o.toString());
                        }
                        session.getTransaction().commit();
                    }
                }
            });
            
        } catch (HibernateException he) {
            he.printStackTrace();
        }
    }
    private void searchCars()
    {
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            //String query = "from Samochod";
            searchQuery = "from Samochod s where s.status=1";
            if(jComboBox2.getSelectedIndex()!=0)
            {
                searchQuery += " and s.marka='" + jComboBox2.getSelectedItem() + "'";
                if(jComboBox3.getSelectedIndex()!=0)
                {
                    searchQuery += " and s.model='" + jComboBox3.getSelectedItem() + "'";
                }
                
            }
            Query q = session.createQuery(searchQuery);
            List resultList = q.list();
            ArrayList<Samochod> plList = (ArrayList)resultList;
            
            selectedYear(plList);
            
            displayResults(plList);
            carList = (ArrayList)plList;
            session.getTransaction().commit();
        } catch (HibernateException he) {
            he.printStackTrace();
        }
    }
    private void selectedYear(ArrayList<Samochod> plList)
    {
        if(jComboBox4.getSelectedIndex()!=0)
            {
                int minDate = Integer.parseInt(jComboBox4.getSelectedItem().toString());
                for(int i=plList.size()-1; i>=0; i--)
                {
                    if(minDate>plList.get(i).getRokProdukcjiInt())
                    {
                        plList.remove(i);
                    }
                }
  
            }
            
            if(jComboBox5.getSelectedIndex()!=0)
            {
                int maxDate = Integer.parseInt(jComboBox5.getSelectedItem().toString());
                for(int i=plList.size()-1; i>=0; i--)
                {
                    if(maxDate<plList.get(i).getRokProdukcjiInt())
                    {
                        plList.remove(i);
                    }
                }
            }
             
    }
    private void displayResults(List resultList)
    {
            DefaultTableModel model = (DefaultTableModel)jTable1.getModel();
            //DefaultTableModel dtm = (DefaultTableModel) jtMyTable.getModel();
            model.setRowCount(0);
            for(Object o : resultList) 
            {
                Samochod s = (Samochod)o;
               // carList.add(s);
                model.addRow(new Object[]{s.getMarka(), s.getModel(), s.getRejestracja(), s.getRokProdukcjiInt(), s.getRodzajPaliwa()});
            }
    }
    private void selectCar()
    {
            try {
            
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            //Samochod s = (Samochod)session.load(Samochod.class, selectedCar+1); load - wybiera w kolejnosci, get - po primary key
            //chosenCar = s;
            Query q = session.createQuery(searchQuery);
            System.out.println("Query = "  + searchQuery);
            List resultList = q.list();
            ArrayList<Samochod> plList = (ArrayList)resultList;
            selectedYear(plList);
            if(sortType != -1)
            {
                getSortType(plList);
            }
            Samochod s = plList.get(jTable1.getSelectedRow());
            System.out.println("Wybrany samochod = "  + s.getId() + " " + s.getMarka() + " " + s.getModel());
            //System.out.println("Wybrany samochod = "  + s1.getId() + " " + s1.getMarka() + " " + s1.getModel());
            session.save(new Wypozyczenie(k,s,Calendar.getInstance().getTime()));
            Query queryUpdate = session.createSQLQuery("update Samochod set status = :status" +
    				" where id = :id");
            queryUpdate.setParameter("status", 2);
            queryUpdate.setParameter("id", s.getId());
            queryUpdate.executeUpdate();
            session.getTransaction().commit();
            JOptionPane.showMessageDialog(null, "Wypożyczono samochód: " + s.getMarka() + " " + s.getModel() + "Nr rejestracji: " + s.getRejestracja(), "Udane wypożyczenie", JOptionPane.PLAIN_MESSAGE);
            this.setVisible(false);
            this.dispose();
            //session.close();
 
            } catch (HibernateException he)
            {
                he.printStackTrace();
            }
            
       
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jComboBox1 = new javax.swing.JComboBox<>();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        jComboBox2 = new javax.swing.JComboBox<>();
        jLabel5 = new javax.swing.JLabel();
        jComboBox3 = new javax.swing.JComboBox<>();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jComboBox4 = new javax.swing.JComboBox<>();
        jLabel8 = new javax.swing.JLabel();
        jComboBox5 = new javax.swing.JComboBox<>();
        jButton2 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel1.setText("Wybierz dostępny samochód:");

        jTable1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null}
            },
            new String [] {
                "Marka", "Model", "Rejestracja", "Rok produkcji", "Rodzaj paliwa"
            }
        ));
        jTable1.setRowHeight(26);
        jScrollPane1.setViewportView(jTable1);

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Marka", "Rok produkcji (rosnąco)", "Rok produkcji (malejąco)" }));

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel2.setText("Sortuj według:");

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel3.setText("Wyszukiwanie:");

        jButton1.setText("Szukaj");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel4.setText("Marka:");

        jComboBox2.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Dowolny" }));

        jLabel5.setText("Model:");

        jComboBox3.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Dowolny" }));
        jComboBox3.setEnabled(false);

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel6.setText("Rok produkcji");

        jLabel7.setText("od:");

        jComboBox4.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "" }));

        jLabel8.setText("do:");

        jComboBox5.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "" }));

        jButton2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jButton2.setText("Wypożycz wybrany samochod");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addGap(18, 18, 18)
                                .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabel3))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jComboBox3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel7)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jComboBox4, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jComboBox5, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 48, Short.MAX_VALUE)
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(199, 199, 199)
                .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 301, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(26, 26, 26)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addGap(28, 28, 28)
                .addComponent(jLabel3)
                .addGap(30, 30, 30)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5)
                    .addComponent(jComboBox3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6)
                    .addComponent(jLabel7)
                    .addComponent(jComboBox4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8)
                    .addComponent(jComboBox5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1))
                .addGap(26, 26, 26)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 290, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(31, 31, 31)
                .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(24, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        searchCars();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        int selectedCar = jTable1.getSelectedRow();
        if(selectedCar != -1)
        {
            selectCar();
        }
        else
        {
            JOptionPane.showMessageDialog(null, "Wybierz samochód", "Błąd", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_jButton2ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(WypozyczSamochod.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(WypozyczSamochod.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(WypozyczSamochod.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(WypozyczSamochod.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new WypozyczSamochod(k).setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JComboBox<String> jComboBox2;
    private javax.swing.JComboBox<String> jComboBox3;
    private javax.swing.JComboBox<String> jComboBox4;
    private javax.swing.JComboBox<String> jComboBox5;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    // End of variables declaration//GEN-END:variables
}
